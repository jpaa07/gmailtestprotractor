import googlePage from '../../src/pages/google.page';
import loginPage from '../../src/pages/login.page';
import inboxPage from '../../src/pages/inbox.page';
import searchData from '../resources/data/searchData';
import userData from '../resources/data/userData';
import errorsData from '../resources/data/errorsData';
import { browser } from 'protractor';

describe ('gmail login', () => {

    const validUser = userData.validAccount;
    const invalidUser = userData.invalidAccount;
    const googleSearch = searchData.googleSearch;
    const error = errorsData.errors;

    beforeAll(() => {
        browser.get(googlePage.url);
        googlePage.search(googleSearch.searchText,googleSearch.selectLink);
        loginPage.enterUsername(validUser.username);
    })

    it('Should display error message for invalid password', () => {
        loginPage.enterPassword(invalidUser.password);
        expect(loginPage.errorMessageLabel.getText()).toEqual(error.wrongPassword);
    });
    
    it('Should go to inbox page on successful login', () => {
        loginPage.passwordInput.clear();
        loginPage.enterPassword(validUser.password);
        expect(inboxPage.isLoaded()).toBe(true);
    });
});