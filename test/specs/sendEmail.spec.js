import loginPage from '../../src/pages/login.page';
import inboxPage from '../../src/pages/inbox.page';
import emailPage from '../../src/pages/email.page';
import googlePage from '../../src/pages/google.page';
import sentMailPage from '../../src/pages/sentMail.page'
import userData from '../resources/data/userData';
import emailData from '../resources/data/emailData';
import searchData from '../resources/data/searchData';
import errorsData from '../resources/data/errorsData';
import { browser } from 'protractor';

describe ('send email on gmail sendEmail', () => {

    const email = emailData.email;
    const googleSearch = searchData.googleSearch;
    const error = errorsData.errors; 
    const validUser = userData.validAccount;
    const invalidUser = userData.invalidAccount;

    beforeAll(() => {
        browser.get(googlePage.url);
        googlePage.search(googleSearch.searchText,googleSearch.selectLink);
        expect(loginPage.isLoaded()).toBe(true);
        loginPage.enterUsername(validUser.username);
        loginPage.enterPassword(validUser.password);
        expect(inboxPage.isLoaded()).toBe(true);
    });

    beforeEach(() => {
        inboxPage.goToComposeMail();
        expect(emailPage.isLoaded()).toBe(true);
    });

    it('Should display pop-up window error message for not recipient entered', () => {
        emailPage.sendEmail("",email.subject,email.body);
        emailPage.waitElementUntilVisible(emailPage.emailErrorLabel);
        expect(emailPage.emailErrorLabel.getText()).toEqual(error.noRecipient);
        emailPage.enter();
        emailPage.waitElementUntilClickable(emailPage.discardMailButton);
        emailPage.discardEmail();
    });

    it('Should display pop-up window error message for invalid recipient', () => {
        emailPage.sendEmail(email.invalidRecipient,email.subject,email.body);
        emailPage.waitElementUntilVisible(emailPage.emailErrorLabel);
        expect(emailPage.emailErrorLabel.getText()).toEqual(error.invalidRecipient.replace("invalidRecipient",email.invalidRecipient));
        emailPage.enter();
        emailPage.waitElementUntilClickable(emailPage.discardMailButton);
        emailPage.discardEmail();
    });    

    it('Should send a email successfully', () => {
        emailPage.sendEmail(email.recipient,email.subject,email.body);
        inboxPage.goToSentEmail();
        expect(sentMailPage.isLoaded()).toBe(true);
        sentMailPage.deleteSentEmail();
        expect(sentMailPage.deleteNotification.isDisplayed()).toBe(true);
    });

    afterAll(() =>{
        inboxPage.signOut();
        expect(loginPage.isLoaded()).toBe(true);
    });
});