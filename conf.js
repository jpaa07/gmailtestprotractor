require("babel-register")({
    presets: [ 'es2015' ]
});

exports.config = {

    directConnect: true,
    specs: ['test/specs/*.spec.js'],
    baseUrl: 'http://google.com',
    framework: 'jasmine',

    onPrepare: () => {
        browser.manage().timeouts().implicitlyWait(35000);
        const SpecReporter = require('jasmine-spec-reporter');
        jasmine.getEnv().addReporter(new SpecReporter({displayStacktrace: 'specs'}));
    },

    capabilities: {
        browserName: 'chrome',
        shardTestFiles: true,
        maxInstances: 1,
        chromeOptions: {
            args: [
                '--disable-infobars',
                '--disable-extensions',
                'verbose',
                '--incognito',
                '--test-type',
                '--ignore-certificate-errors',
                '--allow-running-insecure-content',
                '--disk-cache-dir=null',
                '--start-maximized',
                'log-path=/tmp/chromedriver.log'
            ],
            prefs: {
                'profile.password_manager_enabled': false,
                'credentials_enable_service': false,
                'password_manager_enabled': false
            }
        }
    },

    jasmineNodeOpts: {
        showColors: true,
        displaySpecDuration: true,
        print: () => {},
        defaultTimeoutInterval: 90000
    }
};