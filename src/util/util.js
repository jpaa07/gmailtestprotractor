const base64 = require('base-64');

class Util {
    base64Encode(decodedText){
        return base64.encode(decodedText);
    }
    
    base64Decode(encodedText){
        return base64.decode(encodedText);
    }
}

export default new Util();