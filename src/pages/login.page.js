browser.ignoreSynchronization = true;
import MainPage from './main.page';
import util from '../util/util';
import { browser } from 'protractor';

class LoginPage extends MainPage{
    constructor(){
        super();
        this.usernameInput = element(by.name('identifier'));
        this.passwordInput = element(by.name('password'));
        this.errorMessageLabel = $('.dEOOab');
        this.pageLoaded = this.waitElementUntilPresent($('#headingText')); 
    }

    enterUsername(username){
        this.usernameInput.sendKeys(username);
        return this.enter();
    }

    enterPassword(password){
        this.passwordInput.sendKeys(util.base64Decode(password));
        this.enter();
        return browser.sleep(this.timeout.littleWait);
    }
}

export default new LoginPage();