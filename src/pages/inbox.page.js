browser.ignoreSynchronization = true;
import MainPage from './main.page';

class InboxPage extends MainPage {
    constructor(){
        super();
        this.composeButton = $('.z0 > div');
        this.sentMailLink = $('#link_vsm');
        this.userMenuButton = $('.gb_ab');
        this.signOutButton = element(by.cssContainingText('a','Sign out'));
        this.pageLoaded = this.waitElementUntilPresent($('.aKw'));
    }

    goToComposeMail(){
        return this.composeButton.click();
    }

    goToSentEmail(){
        return this.sentMailLink.click();   
    }

    signOut(){
        this.userMenuButton.click();
        return this.signOutButton.click();
    }
}

export default new InboxPage();