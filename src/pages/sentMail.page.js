browser.ignoreSynchronization = true;
import MainPage from './main.page';

class SentMailPage extends MainPage{
    constructor(){
        super();
        this.deleteNotification = $('.bofITb');
        this.pageLoaded = this.waitElementUntilPresent($(".hP"));
        this.menuButton = element(by.xpath(".//img[@role = 'menu']"));
        this.deleteButton = element(by.xpath(".//div[contains(.,'Delete')]/img"));
    }

    deleteSentEmail(){
        this.menuButton.click();
        return this.deleteButton.click();
    }
}

export default new SentMailPage();