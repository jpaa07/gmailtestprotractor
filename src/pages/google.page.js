browser.ignoreSynchronization = true;
import MainPage from './main.page';

class GooglePage extends MainPage {
    constructor(){
        super();
        this.searchBarInput = $('.gsfi');
        this.url = "/";
    }

    search(searchText,selectLink){
        const resultLink = element(by.cssContainingText('a',selectLink));
        this.searchBarInput.sendKeys(searchText);
        this.enter();
        return resultLink.click();
    }
}

export default new GooglePage();