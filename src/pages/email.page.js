browser.ignoreSynchronization = true;
import MainPage from './main.page';

class EmailPage extends MainPage {
    constructor(){
        super();
        this.recipientField = element(by.name('to'));
        this.subjectField = $('.aoT');
        this.bodyField = $('.editable');
        this.sendButton = $('.aoO');
        this.emailErrorLabel = $('.Kj-JD-Jz');
        this.okButton = element(by.name('ok'));
        this.discardMailButton = $('.og');
        this.pageLoaded = this.waitElementUntilPresent($('.aYF'));
    }

    sendEmail(recipient,subject,body){
        this.recipientField.sendKeys(recipient);
        this.subjectField.sendKeys(subject);
        this.bodyField.sendKeys(body);
        return this.sendButton.click();
    }
    
    discardEmail(){
        browser.sleep(this.timeout.bitWait);
        this.discardMailButton.click();
        return browser.sleep(this.timeout.bitWait);
    }
}

export default new EmailPage();