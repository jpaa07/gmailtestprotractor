# Gmail test automation with protractor #

This is a protractor project, with some e2e tests. The main objective is to validate the correct operation of sending email on gmail's platform.

## Implemented concepts ##

* Dependency injection
* Page Object Model (POM) Architecture
* OOP paradigm
* Self-validating testing
* Independent testing

## Tools ##

* JavaScript ES6
* Node.js
* npm
* Protractor
* Jasmine
* Json
* GIT

## Requirements ##

* Google Chrome Browser
* Node.js (ES6 syntax requires a version of Node.js >= 6.0.0)

## Setup ##

* Install Node (v6.x.x or later) -> (preferably 8.9.0 or later)
* **clone repository**: git clone https://jpaa07@bitbucket.org/jpaa07/gmailtestprotractor.git
* **install all project dependencies**: npm install (Execute command on the workspace root)

## Run Tests ##

### The next commands have to be executed on the workspace root. ###

* **With npm**: npm test
* **With protractor** : protractor conf.js

## Specs ##
{workspaceFolder}/test/specs

### Login ###

* successful login validation
* invalid password validation

### Send Email ###

* successful email sent validation
* no recipient validation
* invalid recipient validation

## Data ##
{workspaceFolder}/test/resources/data

###All data is manage with json object, with the purpose of have a scalable and sustainable test suite.###

### Email Data ###
+ manage all information that is going to be send to the recipient
	* recipient
    * invalidRecipient
    * subject
    * body
	
### Errors Data ###
+ manage all messages that appear when something is wrong 
	* noRecipient
    * invalidRecipient
    * wrongPassword
 
### Search Data ###
+ manage all information needed for search something in google search engine page 
	* searchText
    * selectLink

### User Data ###
+ manage all Gmail valid and invalid accounts for sign in in the portal
	* username
    * password

